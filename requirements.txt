Django==2.2.6
pytz==2019.2
sqlparse==0.3.0
gunicorn>=19.7.1
whitenoise>=4.1.*
